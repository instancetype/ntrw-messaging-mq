/**
 * Created by instancetype on 7/29/14.
 */
'use strict'

const
  cluster = require('cluster')
, zmq = require('zmq')

if (cluster.isMaster) {
  // Master -
  var workerCount = 0
  let pusher = zmq.socket('push').bind('ipc://filer-pusher.ipc')
    , puller = zmq.socket('pull').bind('ipc://filer-puller.ipc')

  let sendJobs = function() {
    for (let i = 0; i < 30; ++i) {
      // console.log('Job was sent...')
      pusher.send(JSON.stringify(
        { 'number' : i }
      ))
    }
  }

  puller.on('message', function(data) {
    // console.log('puller received message...')
    let message = JSON.parse(data.toString())
    // console.log(message)
    if (message.type === 'ready') {
      workerCount++
      if (workerCount > 2) sendJobs()
    }
    else if (message.type === 'result') {
      console.log(message)
    }
    else {
      console.log('Unable to parse message:', message)
    }
  })

  for (let i = 0; i < 3; ++i) {
    cluster.fork()
    // console.log('Cluster was forked...')
  }
}
else {
  // Worker -
  let workPuller = zmq.socket('pull').connect('ipc://filer-pusher.ipc')
    , workPusher = zmq.socket('push').connect('ipc://filer-puller.ipc')

  workPuller.on('message', function(data) {
    let message = JSON.parse(data.toString())
    message.pid = process.pid
    message.type = 'result'
    workPusher.send(JSON.stringify(message))
  })

  workPusher.send(JSON.stringify(
    { type: 'ready'
    , pid : process.pid
    }
  ))
}